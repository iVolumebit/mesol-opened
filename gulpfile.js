let gulp = require("gulp");
let concat = require('gulp-concat');
let sass = require('gulp-sass');
let postcss = require('gulp-postcss');
let autoprefixer = require('autoprefixer');
let mqpacker = require('css-mqpacker');
let rigger  = require('gulp-rigger');
let plumber = require('gulp-plumber');
let gulpSequence = require('gulp-sequence');
let browserSync = require("browser-sync"),
    reload = browserSync.reload;
let nunjucks = require('gulp-nunjucks');
let babel = require('gulp-babel');

const gulpIf = require('gulp-if');
const uglify = require('gulp-uglify');
const debug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development'; //set NODE_ENV=production && gulp


// Styles task
//******************************************

gulp.task("styles", () => {
    gulp.src('src/scss/styles.scss')
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(plumber())
        .pipe(sass())
        .pipe(
            postcss([autoprefixer({browsers: ['last 2 versions', 'ie >= 10', 'Android >= 4.1', 'Safari >= 8', 'iOS >= 7']}), mqpacker({sort: true})]))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest('../wp-content/themes/mesol/dist/css'))
        .pipe(gulp.dest('frontend/dist/css'))
        .pipe(reload({stream: true}))
});

// Copy
//******************************************

gulp.task('assets', () => {
     return gulp.src('src/assets/**/*.*')
        .pipe(gulp.dest('../wp-content/themes/mesol/dist/assets/'))
        .pipe(gulp.dest('frontend/dist/assets/'))
        .pipe(reload({stream: true}));
});

// Script task
//******************************************

gulp.task('babelit', () => {
    gulp.src('src/js/init.js')
        .pipe(plumber())
        .pipe(rigger())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(debug())
        .pipe(gulp.dest('src/js/babled'));
});

gulp.task('scripts', () => {
     gulp.src(
        [
            'src/js/libs/jquery-3.3.1.min.js',
            'src/js/libs/slick.min.js',
            'src/js/libs/jquery.fancybox.min.js',
            'src/js/libs/jquery.vide.min.js',
            'src/js/libs/masonry.pkgd.min.js',
            'src/js/babled/init.js'
        ])
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(plumber())
        .pipe(concat('core.js'))
        .pipe(debug())
        .pipe(gulp.dest('../wp-content/themes/mesol/dist/js'))
        .pipe(gulp.dest('frontend/dist/js'))
        .pipe(uglify())
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest('dist/js'))
        .pipe(gulp.dest('frontend/dist/js'))
        .pipe(reload({stream: true}));
});

// gulp.task('js:bundle', gulpSequence('babelit', 'scripts'));

gulp.task('js:outer', () => {
   gulp.src(
       [
           'src/js/libs/markerclusterer.js'
       ])
       .pipe(gulp.dest('../wp-content/themes/mesol/dist/js'))
       .pipe(gulp.dest('frontend/dist/js'))
});

// Template build task
//******************************************

gulp.task('html', () => {
     gulp.src('src/templates/*.html')
        .pipe(nunjucks.compile())
        .pipe(gulp.dest('./frontend/'))
        .pipe(reload({stream: true}));
});

// Webserver
//******************************************

gulp.task('webserver', () => browserSync(config));

let config = {
    server: {
        baseDir: "./frontend/"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "bw"
};

// Clean task
//******************************************

gulp.task('cleanImg', () => {
    del('../wp-content/themes/mesol/dist/assets/img/**/*.*');
    del('frontend/dist/assets/img/**/*.*');
} );

// Watch task
//******************************************

gulp.task('watch', () => {
    gulp.watch('src/scss/**/*.*', ['styles']);
    gulp.watch('src/js/init.js', ['babelit']);
    gulp.watch('src/js/components/*.js', ['babelit']);
    gulp.watch('src/js/**/*.js', ['scripts']);
    gulp.watch('src/assets/**/*.*', ['assets']);
    gulp.watch('src/templates/**/*.*', ['html']);
});

// Default task
//******************************************
gulp.task("default",
    gulpSequence('babelit', 'cleanImg', 'assets', 'scripts', ['js:outer', 'styles', 'html'])
);

// Development task
//******************************************
gulp.task('dev',
    gulpSequence('default', 'webserver', 'watch')
);