(function () {

   let eventButton = $('.event__links_item'),
      eventBox = $('.event__box'),
      eventSection = $('.event__content_section'),
      linksBox = $('.event__links');

   let wind = $(window);

   let isScrolled = false,
      headerHeight = $('.header').outerHeight(),
      treshold = 20;

   let EventUI = {

      init() {
         this.addListener();
      },

      addListener(event) {
         eventButton.on('click', this._listeners.sectionToggle);
         wind.on('scroll resize', this._listeners.stickyLinks);
      },

      _listeners: {

         sectionToggle() {

            let scrollStart = eventBox.offset().top;

            let self = $(this),
            buttonIndex = self.index();

            let eventBoxStart = scrollStart - headerHeight - treshold;

            if( self.hasClass('active')) return false;

            // если открыт длинный таб, то скроллим наверх, и только потом меняем таб
            if( isScrolled ){

               $('html, body').animate({scrollTop: eventBoxStart}, 500, function () {

                  self.addClass('active').siblings().removeClass('active');
                  eventSection.eq(buttonIndex).addClass('active').siblings().removeClass('active')

               });

            } else {

               self.addClass('active').siblings().removeClass('active');
               eventSection.eq(buttonIndex).addClass('active').siblings().removeClass('active')

            }

         },

         stickyLinks() {

            let scrollStart = eventBox.offset().top;

            if( wind.outerWidth() < 991) return false;

            // начало скролла
            if( wind.scrollTop() > scrollStart - headerHeight - treshold
               && wind.scrollTop() + linksBox.outerHeight() + headerHeight + treshold
               < eventBox.outerHeight() + scrollStart ){


               if(!linksBox.hasClass('sticked')){

                  linksBox.css({position: 'fixed', top: headerHeight + treshold, bottom: ''})
                     .addClass('sticked').removeClass('stopped starting');

                  isScrolled = true;
               }

            } else if ( wind.scrollTop() + linksBox.outerHeight() + headerHeight + treshold >= eventBox.outerHeight() + scrollStart){
               // фиксируем переключатель внизу блока
               if(!linksBox.hasClass('stopped')){

                  linksBox.css({position: 'absolute', top: '', bottom: 0})
                     .addClass('stopped').removeClass('sticked');

               }

            } else {

               if(!linksBox.hasClass('starting')){

                  linksBox.css({position: '', top: '', bottom: ''})
                     .addClass('starting').removeClass('sticked');

                  isScrolled = false;

               }
            }

         }
      }
   };

   if( eventBox.length ) EventUI.init();

})();