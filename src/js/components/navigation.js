// menu mobile behavior

(() => {

   let menuWrap = $('.nav__wrapper'),
      menuButton = $('.js__mobile-toggle');

   let doc = $(document),
      body = $('body');

   let overlay = $('.js__overlay');

   let MenuUI = {

      initialize() {
         this.addListener();
      },

      addListener(event) {
         menuButton.on('click', event, this._listeners.menuSideToggle); // burger action
         doc.on('click', event, this._listeners.closingUI);
      },

      _listeners: {

         menuSideToggle() {

            if (!menuWrap.hasClass('dropped')) {

               MenuUI.action.menuSideOn();

            } else {

               MenuUI.action.menuSideOff();
            }
         },

         closingUI(event) {
            let target = $(event.target);

            if (!target.is(menuWrap) && !target.parents().is(menuWrap) && !target.is(menuButton) && !target.parents().is(menuButton) && menuWrap.hasClass('dropped')) {
               MenuUI.action.menuSideOff();
            }

         }

      },

      action: {

         menuSideOn() {

            menuWrap.addClass('dropped');
            menuButton.addClass('dropped');
            overlay.addClass('active');
            body.addClass('blocked');

         },

         menuSideOff() {

            menuButton.removeClass('dropped');
            menuWrap.removeClass('dropped');
            overlay.removeClass('active');
            body.removeClass('blocked');

         }
      }
   };

   MenuUI.initialize();

})();