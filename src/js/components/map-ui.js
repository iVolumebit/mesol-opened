(function () {

   let themeData = window.themeData || {
      url: ''
   };

   let objectsPlaces = window.objectsCoordinates || [
      [55.77075199999999, 37.50928499999998, 'conducted'],
      [55.7352978, 37.58429769999998, 'inspected'],
      [55.7152978, 37.54429769999998, 'conducted']
   ];

   // TODO example[1] has structure for inspected object
   // let example = [
   //   `<h2 class="object-info__headline">Отель «Континенталь2»</h2>
   //
   //    <div class="object-info__box">
   //
   //       <div class="object-info__links">
   //          <div class="object-info__links_item">Информация о площадке</div>
   //          <div class="object-info__links_item">Проведенные мероприятия</div>
   //       </div>
   //
   //       <div class="object-info__content">
   //
   //          <div class="object-info__section">
   //
   //             <div class="object-info__desc">
   //
   //                <p class="object-info__caption">Залы/Количество мест/Средняя стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Большой зал, 300 м2/150 мест</span>
   //                      <span class="object-info__text_part">70 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Средний зал, 150 м2/150 мест</span>
   //                      <span class="object-info__text_part">40 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Конференц зал, 90 м2/120 мест</span>
   //                      <span class="object-info__text_part">100 000</span>
   //                   </p>
   //
   //
   //                </div>
   //
   //                <p class="object-info__caption">Питание/стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Шведский стол 150 мест</span>
   //                      <span class="object-info__text_part">5 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Классчическое, за человека</span>
   //                      <span class="object-info__text_part">500</span>
   //                   </p>
   //
   //                </div>
   //
   //                <p class="object-info__caption">Адрес площадки:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">119034, г. Москва, ул. Городецкая 8/7б, оф. 12</span>
   //                   </p>
   //                </div>
   //
   //                <p class="object-info__caption">Транспорт:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Маршрутки: 32, 323А, 455: Тралейбусы: 32, 34, 43</span>
   //                   </p>
   //                </div>
   //
   //             </div>
   //
   //             <div class="object-info__carousel_wrapper js__object-info">
   //
   //                <p class="object-info__carousel_headline">Фото площадки:</p>
   //
   //                <div class="object-info__carousel">
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg2.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg2.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg4.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg4.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg3.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg3.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                </div>
   //
   //             </div>
   //
   //          </div>
   //
   //          <div class="object-info__section">
   //
   //             <div class="object-info__desc">
   //
   //                <div class="object-info__conducted">
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                </div>
   //
   //             </div>
   //
   //          </div>
   //
   //       </div>
   //
   //    </div>`,
   //   `<h2 class="object-info__headline">Отель «Континенталь3»</h2>
   //
   //    <div class="object-info__box">
   //
   //       <div class="object-info__links">
   //          <div class="object-info__links_item">Информация о площадке</div>
   //          <div class="object-info__inspected-marker">инспектирован</div>
   //       </div>
   //
   //       <div class="object-info__content">
   //
   //          <div class="object-info__section">
   //
   //             <div class="object-info__desc">
   //
   //                <p class="object-info__caption">Залы/Количество мест/Средняя стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Большой зал, 300 м2/150 мест</span>
   //                      <span class="object-info__text_part">70 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Средний зал, 150 м2/150 мест</span>
   //                      <span class="object-info__text_part">40 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Конференц зал, 90 м2/120 мест</span>
   //                      <span class="object-info__text_part">100 000</span>
   //                   </p>
   //
   //
   //                </div>
   //
   //                <p class="object-info__caption">Питание/стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Шведский стол 150 мест</span>
   //                      <span class="object-info__text_part">5 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Классчическое, за человека</span>
   //                      <span class="object-info__text_part">500</span>
   //                   </p>
   //
   //                </div>
   //
   //                <p class="object-info__caption">Адрес площадки:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">119034, г. Москва, ул. Городецкая 8/7б, оф. 12</span>
   //                   </p>
   //                </div>
   //
   //                <p class="object-info__caption">Транспорт:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Маршрутки: 32, 323А, 455: Тралейбусы: 32, 34, 43</span>
   //                   </p>
   //                </div>
   //
   //             </div>
   //
   //          </div>
   //
   //       </div>
   //
   //    </div>`,
   //   `<h2 class="object-info__headline">Отель «Континенталь4»</h2>
   //
   //    <div class="object-info__box">
   //
   //       <div class="object-info__links">
   //          <div class="object-info__links_item">Информация о площадке</div>
   //          <div class="object-info__links_item">Проведенные мероприятия</div>
   //       </div>
   //
   //       <div class="object-info__content">
   //
   //          <div class="object-info__section">
   //
   //             <div class="object-info__desc">
   //
   //                <p class="object-info__caption">Залы/Количество мест/Средняя стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Большой зал, 300 м2/150 мест</span>
   //                      <span class="object-info__text_part">70 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Средний зал, 150 м2/150 мест</span>
   //                      <span class="object-info__text_part">40 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Конференц зал, 90 м2/120 мест</span>
   //                      <span class="object-info__text_part">100 000</span>
   //                   </p>
   //
   //
   //                </div>
   //
   //                <p class="object-info__caption">Питание/стоимость, руб</p>
   //                <div class="object-info__desc_section">
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Шведский стол 150 мест</span>
   //                      <span class="object-info__text_part">5 000</span>
   //                   </p>
   //
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Классчическое, за человека</span>
   //                      <span class="object-info__text_part">500</span>
   //                   </p>
   //
   //                </div>
   //
   //                <p class="object-info__caption">Адрес площадки:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">119034, г. Москва, ул. Городецкая 8/7б, оф. 12</span>
   //                   </p>
   //                </div>
   //
   //                <p class="object-info__caption">Транспорт:</p>
   //                <div class="object-info__desc_section">
   //                   <p class="object-info__text">
   //                      <span class="object-info__text_part">Маршрутки: 32, 323А, 455: Тралейбусы: 32, 34, 43</span>
   //                   </p>
   //                </div>
   //
   //             </div>
   //
   //             <div class="object-info__carousel_wrapper js__object-info">
   //
   //                <p class="object-info__carousel_headline">Фото площадки:</p>
   //
   //                <div class="object-info__carousel">
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg1.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg1.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg2.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg2.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                   <div class="object-info__carousel_item">
   //                      <a href="dist/assets/img/main/main-bg3.jpg" class="object-info__carousel_figure" data-fancybox="objects">
   //                         <img src="dist/assets/img/main/main-bg3.jpg" alt="" class="object-info__carousel_view">
   //                      </a>
   //                   </div>
   //
   //                </div>
   //
   //             </div>
   //
   //          </div>
   //
   //          <div class="object-info__section">
   //
   //             <div class="object-info__desc">
   //
   //                <div class="object-info__conducted">
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">Конференция глобального потепления</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                   <div class="object-info__conducted_item">
   //                      <p class="object-info__conducted_name">II Всероссийская конференция</p>
   //                      <a href="" class="object-info__conducted_link">Перейти в портфолио</a>
   //                   </div>
   //
   //                </div>
   //
   //             </div>
   //
   //          </div>
   //
   //       </div>
   //
   //    </div>`
   // ];

   let wind = $(window);
   let objectInfoArea = $('.js__object-info'), // object info area
      header = $('.header'),
      mapBox = wind.outerWidth() >= 768 ? $('.map') : $('.map__interface'); // get area to show a preloader


   let currentObject; // current object's index

   let MapTabUI = {

      init() {
         this.addListener();
      },

      addListener(event) {
         this._listeners.initial(); //tab && carousel initial
         objectInfoArea.on('click', '.object-info__links_item', this._listeners.sectionToggle); // tab button handler
      },

      _listeners: {

         initial () {

            objectInfoArea.find('.object-info__links_item').eq(0).addClass('active');
            objectInfoArea.find('.object-info__section').eq(0).addClass('active');

            objectInfoArea.find('.object-info__carousel').slick(common.carouselMapSet);

         },

         sectionToggle() {

            let self = $(this),
               buttonIndex = self.index();

            if( self.hasClass('active')) return false;

            self.addClass('active').siblings().removeClass('active');
            objectInfoArea.find('.object-info__section').eq(buttonIndex).addClass('active')
               .siblings().removeClass('active');

         }
      }
   };

   if( objectInfoArea.length ) MapTabUI.init();

   function getLocationInfo(index) {

      if (currentObject === index) return false;

      $('.object-info__carousel').slick('unslick');
      mapBox.addClass('loading');

      // local realization
      // setTimeout(function () {
      //    objectInfoArea.html(window.areasInfo[index] || example[index]);
      //    MapTabUI._listeners.initial();
      //    mapBox.removeClass('loading');
      //    currentObject = index;
      //
      //    wind.outerWidth() < 768 ?
      //       $('html, body').animate({scrollTop: objectInfoArea.offset().top - header.outerHeight() + 30}, 500) :
      //       0;
      // }, 2000);

      $.ajax({
         type: 'POST',
         url: myajax.url,
         data: {objectIndex: index, action: 'get_area_data'}, // get next object by index

         success (data) {
            objectInfoArea.html(data);
            MapTabUI._listeners.initial();
            mapBox.removeClass('loading');
            currentObject = index;

            wind.outerWidth() < 768 ?
               $('html, body').animate({scrollTop: objectInfoArea.offset().top - header.outerHeight() + 30}, 500) :
               0;
         },

         error (xhr, str){
            setTimeout(function () {
               mapBox.removeClass('loading');
            }, 2000);
            console.log("Error: "+ xhr.responseCode);
         }
      });
   }

   function initialize() {
      let mapOptions = {
         scrollwheel: true,
         clickableIcons: false
      };

      let customMapType = new google.maps.StyledMapType([
         {
            featureType: "all",
            stylers: [
               { saturation: -80 }
            ]
         },{
            featureType: "road.arterial",
            elementType: "geometry",
            stylers: [
               { hue: "#00ffee" },
               { saturation: 50 }
            ]
         },{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [
               { visibility: "off" }
            ]
         }
      ], {
         name: 'Custom Style'
      });

      let customMapTypeId = 'custom_style';

      let map = new google.maps.Map(document.getElementById('map-object'), mapOptions);

      map.mapTypes.set(customMapTypeId, customMapType);
      map.setMapTypeId(customMapTypeId);

      let pinInspected = {
            url: themeData.url + '/dist/assets/img/map/pin-inspected.svg',
            scaledSize: new google.maps.Size(44, 44)
         },
         pinInspectedActive = {
            url: themeData.url + '/dist/assets/img/map/pin-inspected-active.svg',
            scaledSize: new google.maps.Size(44, 44)
         },
         pinConducted = {
            url: themeData.url + '/dist/assets/img/map/pin-event.svg',
            scaledSize: new google.maps.Size(44, 44)
         },
         pinConductedActive = {
            url: themeData.url + '/dist/assets/img/map/pin-event-active.svg',
            scaledSize: new google.maps.Size(44, 44)
         }
      ;

      let markers = [];
      let latlngbounds = new google.maps.LatLngBounds();


      for( let i = 0; i < objectsPlaces.length; i++ ){

         let object = objectsPlaces[i],
            lat = object[0],
            lng = object[1],
            objectType = object[2];

         let marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            icon: objectType === 'conducted' ? pinConducted : pinInspected,
            id: i,
            iconType: objectType
         });

         latlngbounds.extend(new google.maps.LatLng(lat, lng));

         markers.push(marker);

         marker.addListener('click', function() {

            let objectIndex = this.id;

            for (let i = 0; i < markers.length; i++) {

               let clickedMarker = markers[i];

               if (clickedMarker.id === marker.id) {

                  // market which has been clicked
                  clickedMarker.iconType === 'conducted' ?
                  clickedMarker.setIcon(pinConductedActive) :
                  clickedMarker.setIcon(pinInspectedActive);

               } else {

                  // others markers
                  clickedMarker.iconType === 'conducted' ?
                     clickedMarker.setIcon(pinConducted) :
                     clickedMarker.setIcon(pinInspected);
               }

            }

            getLocationInfo(objectIndex);

         });

      }

      let options = {
         imagePath: themeData.url + '/dist/assets/img/map/m'
      };

      map.fitBounds( latlngbounds );

      let markerCluster = new MarkerClusterer(map, markers, options);

   }

   if( $('#map-object').length ) initialize();

})();