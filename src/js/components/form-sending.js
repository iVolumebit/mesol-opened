(function () {

   let form = $('.js__form-feedback'),
      fieldset = $('.form__label'),
      formButton = form.find('.button');

   let checkingSet = {
      form: form,
      fieldset: fieldset,
      el: fieldset.children()
   };

   let sendFormUI = {

      initialize() {
         this.addListener();
      },

      addListener(event) {
         form.on('submit', event, this._listeners.formSubmit); // burger action
         fieldset.children().on('change keyup', this._listeners.updateValidStatus);
      },

      _listeners: {

         formSubmit(event) {

            event.preventDefault();

            let data = form.serialize();

            if (!Checking.fieldsChecking(checkingSet)) return false;
            if (formButton.hasClass('loading')) return false;
            formButton.addClass('loading');

            isSubmitted = false;

            $.ajax({
               type: 'POST',
               url: myajax.url,
               data: data,

               success: function (data) {

                  form.find('input:not([type=hidden])').val('');

                  $.fancybox.close();
                  $.fancybox.open({
                     src: '#thx-message'
                  }, common.modal);

                  formButton.removeClass('loading');

               },
               error: function (xhr, str) {
                  console.log("Error: " + xhr.responseCode);
                  formButton.removeClass('loading');
               }
            });

         },

         updateValidStatus() {

            Checking.singleFieldChecking($(this)); // снимаем ошибку для отдельного поля после хотя бы одного символа
         }
      }
   };

   sendFormUI.initialize();

})();