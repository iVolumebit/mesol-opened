// checking

window.Checking = {

   fieldsChecking(checkingSet) {

      let form = checkingSet.form,
         fieldset = checkingSet.fieldset,
         el = checkingSet.el,
         warningEl = checkingSet.warningEl || false,
         modal = checkingSet.modal || false;

      let fieldStatus = true;

      form.find(fieldset).each(function () {

         $(this).find(el).each(function () {

            if ($(this).hasClass('req')) {

               if ($(this).attr('type') === 'radio') {

                  if ($(this).prop('checked') === false) {

                     $(this).parent().addClass('error').closest(fieldset).attr('data-error', 'error');

                  } else {

                     $(this).closest(fieldset, form).find(el).each(function () {

                        $(this).parent().removeClass('error').addBack().removeClass('req').closest(fieldset).attr('data-error', 'true');

                     });
                  }

               } else if ($(this).is('select')) {

                  // специально для chosen
                  if ($(this).val() === '' || $(this).val() === null) {

                     $(this).addClass('error').closest(fieldset).attr('data-error', 'error');

                  } else {

                     $(this).next().removeClass('error');

                  }

               } else {

                  if ($(this).attr('data-type')) {

                     if ($(this).attr('data-type') === 'email') {

                        if (!$(this).val().match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/)) {

                           $(this).addClass('error').closest(fieldset).attr('data-error', 'error');

                        }

                     } else if ($(this).attr('data-type') === 'decimal') {

                        if ($(this).val() === '' || !$(this).val().match(/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/)) {

                           $(this).addClass('error').closest(fieldset).attr('data-error', 'error');

                        }

                     }

                  } else {

                     if ($.trim($(this).val()) === '') {

                        $(this).addClass('error').closest(fieldset).attr('data-error', 'error');

                     } else {

                        $(this).removeClass('error');

                     }

                  }

               }
            }
         });

         if ($(this).attr('data-error') === 'error') {

            fieldStatus = false;

         }

      });

      if ($('.error').length) {

         fieldStatus = false;

         if (!checkingSet.modal) {

            if ($('.error').is('input') || $('.error').is('select')) {
               $('.error').first().focus();
            }

         } else {

            $('html, body').animate({scrollTop: $('.error').first().offset().top - 100}, 500, function () {
               if ($('.error').is('input') || $('.error').is('select')) {
                  $('.error').first().focus();
               }
            });

         }

      } else {

         fieldStatus = true;

      }

      if (warningEl) {

         if (!fieldStatus) {

            if (warningEl.hasClass('active')) {

               warningEl.removeClass('active');

               setTimeout(function () {
                  warningEl.addClass('active')
               }, 300);

            } else warningEl.addClass('active');

         } else {

            warningEl.removeClass('active');

         }

      }

      return fieldStatus;

   },

   singleFieldChecking(field) {

      if (field.hasClass('req')) {

         $.trim(field.val()) === '' ? field.addClass('error') : field.removeClass('error');

      }

   }

};