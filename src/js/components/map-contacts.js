(function () {

   function initialize() {

      let markerSet = {
         url:'',
         marker: {
            lat: '55.7396933',
            lng: '37.86669389999997'
         }
      };

      if (location.hostname === "localhost") themeData = markerSet;

      let lat = themeData.marker.lat,
         lng = themeData.marker.lng;

      let myLatlng = new google.maps.LatLng(lat, lng);

      let mapOptions = {
         zoom: 15,
         center: myLatlng,
         disableDefaultUI: false,
         scrollwheel: true
      };

      let customMapType = new google.maps.StyledMapType([
         {
            featureType: "all",
            stylers: [
               { saturation: -80 }
            ]
         },{
            featureType: "road.arterial",
            elementType: "geometry",
            stylers: [
               { hue: "#00ffee" },
               { saturation: 50 }
            ]
         },{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [
               { visibility: "off" }
            ]
         }
      ], {
         name: 'Custom Style'
      });
      let customMapTypeId = 'custom_style';

      let map = new google.maps.Map(document.getElementById('contacts'), mapOptions);

      let pin = {
            url: themeData.url + '/dist/assets/img/map/pin-event-active.svg',
            scaledSize: new google.maps.Size(52, 70)
         };

      let marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         icon: pin,
         title: 'MESOL'
      });

      map.mapTypes.set(customMapTypeId, customMapType);
      map.setMapTypeId(customMapTypeId);

   }

   if ( $('#contacts').length ) initialize();

})();