(function () {

   let links = window.links || {
      mp4: 'dist/assets/video/video',
      webm: 'dist/assets/video/video',
      ogv: 'dist/assets/video/video'
   };

   window.common = {

      modal: {
         baseClass: 'layout_blur',
         mobile: {
            margin: 0
         }
      },

      modalDefault: {
         mobile: {
            margin: 0
         }
      },

      carouselCaseSet: {
         infinite: true,
         slidesToShow: 3,
         slidesToScroll: 3,
         autoplay: false,
         accessibility: false,
         dots: false,
         adaptiveHeight: false,
         arrows: true,
         responsive: [
            {
               breakpoint: 992,
               settings: {
                  arrows: false,
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  dots: true,
                  appendDots: '.event-more__dots'
               }
            },

            {
               breakpoint: 768,
               settings: {
                  arrows: false,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: true,
                  appendDots: '.event-more__dots'

               }
            }
         ]
      },

      carouselMapSet: {
         infinite: false,
         slidesToShow: 2,
         slidesToScroll: 2,
         autoplay: false,
         accessibility: false,
         dots: false,
         adaptiveHeight: false,
         arrows: true,
         responsive: [

            {
               breakpoint: 992,
               settings: {
                  arrows: true,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: false
               }
            }
         ]
      }
   };

   $('[data-feedback]').fancybox(common.modal);
   $('[data-fancybox]').fancybox(common.modalDefault);

   $('.js__carousel-case').slick(common.carouselCaseSet);

   if( $(window).outerWidth() >= 992 ){

      $('.fscreen__video_item').vide({
         mp4: links.mp4,
         webm: links.webm,
         ogv: links.ogv
      },{
         muted: true,
         loop: true,
         autoplay: true,
         posterType: "none"
      })
   }

})();