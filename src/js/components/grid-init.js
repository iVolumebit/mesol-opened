(function (event) {

   let optionsServices = {
      itemSelector: '.services__item',
      percentPosition: true,
      horizontalOrder: true,
      gutter: 30
   };

   let optionsCatalog = {
      itemSelector: '.catalog__item',
      columnWidth: '.catalog__item_sizer',
      percentPosition: true,
      horizontalOrder: true,
      gutter: 30,
      stamp: '.catalog__headline'
   };

   let containerServices = $('.services__box'),
      containerCatalog = $('.catalog__box');

   let options = containerServices.length ? optionsServices : optionsCatalog;

   // el wich will be contained for masonry grid
   let gridAnchor = containerServices.length ? containerServices : containerCatalog,
      gridBox,
      isActive = false;

   // turns on the masonry only for desktop and then watch for resize
   function letsGrid(event, el) {

      let initialEl = el || event.data.el;

      if ( $(window).outerWidth() >= 992){

         if(!isActive) {
            gridBox = initialEl.masonry(options);
            isActive = true;
         }

      } else {

         if(isActive){
            gridBox.masonry('destroy');
            isActive = false;
         }

      }

   }

   if( gridAnchor.length ){

      letsGrid(event, gridAnchor);
      $(window).on('resize', event, {el: gridAnchor}, letsGrid);

   }

})();